#include "filemanager.h"

int fileBackup(TCHAR src[], TCHAR dst[], Log* log) {
	setlocale(LC_ALL, "");
	WIN32_FIND_DATA ffdSrc;
	WIN32_FIND_DATA ffdDst;
	HANDLE hSrc = INVALID_HANDLE_VALUE;
	HANDLE hDst = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;
	TCHAR tempSrc[MAX_PATH], tempDst[MAX_PATH];

	// Prepare strings for use with FindFile functions (append '*' to directory names).
	TCHAR newSrc[MAX_PATH], newDst[MAX_PATH];
	getNewPath(newSrc, src, newDst, dst, _T("*"), NULL);

	// Find the first file in each directory.
	hSrc = FindFirstFile(newSrc, &ffdSrc);
	hDst = FindFirstFile(newDst, &ffdDst);

	if (hSrc == INVALID_HANDLE_VALUE) {
		cout << "The source directory does not exist" << endl;
		return dwError;
	}

	if (hDst == INVALID_HANDLE_VALUE) {
		createFolder(hDst, ffdDst, dst);
	}

	// Backup all new/changed files in the directory.
	do {
		if (!_tcscmp(ffdSrc.cFileName, _T(".")) || !_tcscmp(ffdSrc.cFileName, _T(".."))) {
			FindNextFile(hDst, &ffdDst);
			continue;
		}

		while ((_tcscmp(ffdSrc.cFileName, ffdDst.cFileName) < 0) && (FindNextFile(hDst, &ffdDst) != 0));

		if (ffdSrc.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			getNewPath(tempSrc, src, tempDst, dst, ffdSrc.cFileName, _T("\\"));
			dwError = fileBackup(tempSrc, tempDst, log);

			FindNextFile(hDst, &ffdDst);
		}
		else {
			if (_tcscmp(ffdSrc.cFileName, ffdDst.cFileName)) {
				getNewPath(tempSrc, src, tempDst, dst, ffdSrc.cFileName, NULL);
				backUpFile(tempSrc, tempDst, ffdSrc.cFileName, TRUE, log);
			}

			else {
				if (CompareFileTime(&ffdSrc.ftLastWriteTime, &ffdDst.ftLastWriteTime)) {
					getNewPath(tempSrc, src, tempDst, dst, ffdSrc.cFileName, NULL);
					backUpFile(tempSrc, tempDst, ffdSrc.cFileName, FALSE, log);
				}
				FindNextFile(hDst, &ffdDst);
			}
		}
	} while (FindNextFile(hSrc, &ffdSrc) != 0);


	dwError = GetLastError();

	FindClose(hSrc);
	FindClose(hDst);

	return dwError;
}

void getNewPath(TCHAR newSrc[], TCHAR src[], TCHAR newDst[], TCHAR dst[], TCHAR add1[], TCHAR add2[]) {
	StringCchCopy(newSrc, MAX_PATH, src);
	StringCchCat(newSrc, MAX_PATH, add1);
	StringCchCopy(newDst, MAX_PATH, dst);
	StringCchCat(newDst, MAX_PATH, add1);
	if (add2) {
		StringCchCat(newSrc, MAX_PATH, add2);
		StringCchCat(newDst, MAX_PATH, add2);
	}
}

void backUpFile(TCHAR src[], TCHAR dst[], TCHAR file[], bool overwrite, Log* log) {
	if (!CopyFile(src, dst, overwrite))
		tcout << "Failed to copy " << file << " file" << endl;
	else
		log->Write(dst);
}

void createFolder(HANDLE &hDst, WIN32_FIND_DATA &ffdDst, TCHAR dst[]) {
	TCHAR *tok;
	TCHAR path[MAX_PATH];
	TCHAR newDst[MAX_PATH];
	TCHAR *next_token = NULL;

	StringCchCopy(newDst, MAX_PATH, dst);
	tok = wcstok_s(newDst, _T("\\"), &next_token);
	StringCchCopy(path, MAX_PATH, tok);
	StringCchCat(path, MAX_PATH, _T("\\"));

	while (_tcscmp(next_token, L"")) {
		tok = wcstok_s(NULL, _T("\\"), &next_token);
		StringCchCat(path, MAX_PATH, tok);

		hDst = FindFirstFile(path, &ffdDst);

		if (hDst == INVALID_HANDLE_VALUE)
			if (!CreateDirectory(path, NULL))
				tcout << "CreateDirectory failed (" << GetLastError() << ")" << endl;

		StringCchCat(path, MAX_PATH, _T("\\"));
	}

	StringCchCat(path, MAX_PATH, _T("*"));
	hDst = FindFirstFile(path, &ffdDst);
}