#include "filemanager.h"

const tstring getTime() {
	time_t      timer;
	struct tm   tstruct;
	TCHAR       wtime[80] = L"";

	timer = time(NULL);
	localtime_s(&tstruct, &timer);

	// YYYY-MM-DD.HH:mm:ss
	wsprintf(wtime, L"%4d-%02d-%02d %02d:%02d:%02d", tstruct.tm_year + 1900, tstruct.tm_mon + 1,
		tstruct.tm_mday, tstruct.tm_hour, tstruct.tm_min, tstruct.tm_sec);

	return wtime;
}

Log::Log(string filename) {
	m_stream.open(filename, ios::app);
	m_stream.imbue(locale(""));
	m_stream << "=================================================" << endl;
	m_stream << getTime() << "\n" << endl;
}

void Log::Write(tstring logline) {
	m_stream << logline << endl;
}

Log::~Log() {
	m_stream.close();
}