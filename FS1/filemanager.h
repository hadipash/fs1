#pragma once
#include <iostream>
#include <windows.h>
#include <tchar.h>
#include <vector>
#include <string>
#include <strsafe.h>
#include <fstream>
#include <time.h>
#include <sstream>
#include <iomanip>
#include <locale.h>

using namespace std;

typedef std::basic_string<TCHAR>    tstring;
typedef basic_ofstream<TCHAR>       tofstream;

#ifdef UNICODE
#define tcin                        wcin
#define tcout                       wcout
#define tcerr                       wcerr
#define tclog                       wclog
#else
#define tcin                        cin
#define tcout                       cout
#define tcerr                       cerr
#define tclog                       clog
#endif


class Log {
public:
	Log(string filename);
	~Log();
	void Write(tstring logline);
private:
	tofstream m_stream;
};

int fileBackup(TCHAR src[], TCHAR dst[], Log* log);
void getNewPath(TCHAR newSrc[], TCHAR src[], TCHAR newDst[], TCHAR dst[], TCHAR add1[], TCHAR add2[]);
void backUpFile(TCHAR src[], TCHAR dst[], TCHAR file[], bool overwrite, Log* log);
void createFolder(HANDLE &hDst, WIN32_FIND_DATA &ffdDst, TCHAR dst[]);