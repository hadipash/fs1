#include "filemanager.h"

int _tmain(int argc, TCHAR *argv[]) {
	TCHAR src[MAX_PATH], dst[MAX_PATH];
	DWORD dwError = 0;

	// If the directories are not specified as a command-line argument
	if (argc != 3) {
		cout << "Enter source and destination directories" << endl;
		return (-1);
	}

	// Prepare strings for use with FindFile functions.
	// First, copy strings to buffers, then append '\' to directory names.
	StringCchCopy(src, MAX_PATH, argv[1]);
	StringCchCat(src, MAX_PATH, _T("\\"));
	StringCchCopy(dst, MAX_PATH, argv[2]);
	StringCchCat(dst, MAX_PATH, _T("\\"));

	Log log("mybackup.log");

	dwError = fileBackup(src, dst, &log);

	return dwError;
}